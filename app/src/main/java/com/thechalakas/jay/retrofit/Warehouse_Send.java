package com.thechalakas.jay.retrofit;
/*
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Warehouse_Send {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Warehouse_Name")
    @Expose
    private String warehouse_Name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWarehouse_Name() {
        return warehouse_Name;
    }

    public void setWarehouse_Name(String warehouseName) {
        this.warehouse_Name = warehouse_Name;
    }

}